видос https://youtu.be/7SSijTvXk9Y
mail: svssys@ya.ru
код Соколова Сергея

# frontend-basic

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


В этом задании необходимо реализовать простую веб-страницу, используя любой из вышеперечисленных фреймворков.
Требования к заданию:
- сверстать "Hello World" экран со своим именем, фамилией и эмодзи. Дизайн экрана находится на странице "Задание 1" в Figma-макете;
- текст, логотип и эмодзи должны быть представлены разными компонентами (NameText, Emoji, JinguLogo);
- страница должна быть адаптивной;
- добавить небольшую зацикленную анимацию эмодзи на ваше усмотрение;
- использование CSS-препроцессоров или CSS-in-JS библиотек (tailwind, emotion, styled) будет плюсом.

Материалы задания

1. Figma-макет: https://www.jingu.ru/sirius/web-figma
2. Логотип: https://www.jingu.ru/sirius/jingu-logo-512.png
3. Что посмотреть?
4. React Getting started (ru): https://ru.reactjs.org/
5. Vue Getting started: https://vuejs.org/v2/guide/

Примечание
Для демонстрации выполненной работы запишите видео с результатом работы и продемонстрируйте адаптивность страницы. Видео загрузите на YouTube и прикрепите ссылку на него в документе с ответами на задания. Исходный код загрузите на GitHub и прикрепите ссылку на репозиторий. 
Важно! Проверьте доступность ваших ссылок через режим инкогнито в браузере.
